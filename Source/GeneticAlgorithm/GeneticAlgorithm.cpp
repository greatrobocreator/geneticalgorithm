// Fill out your copyright notice in the Description page of Project Settings.


#include "GeneticAlgorithm.h"

UGeneticAlgorithm::UGeneticAlgorithm() {};

/*UGeneticAlgorithm::UGeneticAlgorithm(int genotypeParamCount, int l_populationSize) : populationSize(l_populationSize) {

	Init(genotypeParamCount, l_populationSize);

}*/

void UGeneticAlgorithm::Init(TArray<int>& l_layersSizes, TArray<EActivationFunc>& l_layersFuncs, int l_populationSize, bool l_neat) {

	neat = l_neat;
	populationSize = l_populationSize;
	defRandomCount = (neat ? 0 : populationSize / 20);

	currentPopulation.Init(NULL, populationSize);

	if (!neat) {
		for (int i = 0; i < populationSize; ++i) {
			UGenotypeFC* genotype = NewObject<UGenotypeFC>();
			genotype->Init(l_layersSizes, l_layersFuncs, defInitParamMin, defInitParamMax);
			currentPopulation[i] = genotype;
		}
	}
	else {
		UGenotypeNEAT* genotype = NewObject<UGenotypeNEAT>();
		genotype->Init(l_layersSizes[0], l_layersSizes.Last(), innovations);
		currentPopulation[0] = genotype;
		for(int i = 1; i < populationSize; ++i) {
			UGenotypeNEAT* genotype2 = NewObject<UGenotypeNEAT>();
			genotype2->Init(genotype);
			currentPopulation[i] = genotype2;
		}
	}

	generationCount = 1;
	sortPopulation = true;
	running = false;

	initializePopulation.BindDynamic(this, &UGeneticAlgorithm::defaultPopulationInitialization);
	fitnessCalculation.BindDynamic(this, &UGeneticAlgorithm::defaultFitnessCalculation);
	selection.BindDynamic(this, &UGeneticAlgorithm::defaultSelectionOperator);
	recombination.BindDynamic(this, &UGeneticAlgorithm::defaultRecombinationOperator);
}

void UGeneticAlgorithm::start() {

	running = true;
	initializePopulation.Execute(currentPopulation);
	evaluation.Execute(currentPopulation);

}

void UGeneticAlgorithm::evaluationFinished() {

	// Calculate fitness from evaluation
	fitnessCalculation.Execute(currentPopulation);

	// Sort population if flag was set
	if (sortPopulation) currentPopulation.Sort();// &UGenotype::operator<);

	// Check  termination criterion
	if (terminationCriterion.IsBound() && terminationCriterion.Execute(currentPopulation)) {
		terminate();
		return;
	}

	// Apply selection
	TArray<UGenotype*> intermediatePopulation = selection.Execute(currentPopulation);

	// Apply recombination
	TArray<UGenotype*> newPopulation = recombination.Execute(intermediatePopulation, populationSize);

	// Apply mutation
	mutation.Execute(newPopulation);

	// Set current population to newly generated one and start evaluation again
	currentPopulation = newPopulation;
	generationCount++;

	if(neat) speciate(currentPopulation);

	// Clear temp arrays
	intermediatePopulation.Empty();
	newPopulation.Empty();

	evaluation.Execute(currentPopulation);
}

void UGeneticAlgorithm::terminate() {
	running = false;
}

// Initializes the population by setting each parameter to a random value in the default range
void UGeneticAlgorithm::defaultPopulationInitialization(TArray<UGenotype*>& population) {
	for (UGenotype* genotype : population) {
		genotype->randomizeGenotype(defInitParamMin, defInitParamMax);
	}
	if(neat) speciate(population);
}

// Calculates the fitness of each genotype by the formula: fitness = evaluation / averageEvaluation
void UGeneticAlgorithm::defaultFitnessCalculation(TArray<UGenotype*>& l_currentPopulation) {

	// NEAT evaluation correction
	if (neat) {
		for (int i = 0; i < species.Num(); ++i) {
			species[i].age++;
			for (int j = 0; j < species[i].population.Num(); ++j) {
				if (species[i].population[j]->evaluation > species[i].maxEvaluation) {
					species[i].maxEvaluation = species[i].population[j]->evaluation;
					species[i].lastImprovementAge = species[i].age;
				}
				species[i].population[j]->evaluation /= species[i].population.Num();
				if (species[i].population[j]->evaluation <= 0) species[i].population[j]->evaluation = 0;

				if (species[i].age <= 10) species[i].population[j]->evaluation *= youngBonus;
			}
			if (species[i].age - species[i].lastImprovementAge >= dropoffAge)
				for (int j = 0; j < species[i].population.Num(); ++j)
					species[i].population[j]->evaluation *= 0.01;
		}
	}

	// Calculating fitness from evaluation
	float overallEvaluation = 0;

	for (UGenotype* genotype : l_currentPopulation) {
		overallEvaluation += genotype->evaluation;
	}

	float averageEvaluation = overallEvaluation / l_currentPopulation.Num();

	for (UGenotype* genotype : l_currentPopulation) {
		genotype->fitness = genotype->evaluation / averageEvaluation;
	}
}

// Only selects the best three genotypes of the current population and copies them to the intermediate population
TArray<UGenotype*> UGeneticAlgorithm::defaultSelectionOperator(TArray<UGenotype*>& l_currentPopulation) {
	TArray<UGenotype*> intermediatePopulation;
	for(int i = 0; i < FMath::Min<int>(3, l_currentPopulation.Num()); ++i)
		intermediatePopulation.Add(l_currentPopulation[i]);

	return intermediatePopulation;
}

// Selection operator for the genetic algorithm, using a method called remainder stochastic sampling.
TArray<UGenotype*> UGeneticAlgorithm::remainderStochasticSampling(TArray<UGenotype*>& l_currentPopulation) {

	TArray<UGenotype*> intermediatePopulation;
	
	intermediatePopulation.Add(l_currentPopulation[0]);
	intermediatePopulation.Add(l_currentPopulation[1]);

	//Put integer portion of genotypes into intermediatePopulation
	//Assumes that currentPopulation is already sorted
	for (UGenotype* genotype : l_currentPopulation) {
		for (int i = 0; i < (int) genotype->fitness; ++i) {
			/*UGenotypeFC* genotypeCopy = NewObject<UGenotypeFC>();
			genotypeCopy->Init(genotype);
			intermediatePopulation.Add(genotypeCopy);*/
			intermediatePopulation.Add(genotype);
		}
	}

	//Put remainder portion of genotypes into intermediatePopulation
	for (UGenotype* genotype : l_currentPopulation) {
		float remainder = genotype->fitness - (int) genotype->fitness;
		if(FMath::FRandRange(0, 1) < remainder) {
			/*UGenotypeFC* genotypeCopy = NewObject<UGenotypeFC>();
			genotypeCopy->Init(genotype);
			intermediatePopulation.Add(genotypeCopy);*/
			intermediatePopulation.Add(genotype);
		}
	}
	return intermediatePopulation;
}

TArray<UGenotype*> UGeneticAlgorithm::defaultRecombinationOperator(TArray<UGenotype*>& intermediatePopulation, int newPopulationSize) {
	
	check(intermediatePopulation.Num() >= 2);

	TArray<UGenotype*> newPopulation;
	while (newPopulation.Num() < newPopulationSize) {

		if (!neat) {
			UGenotypeFC* offspring1;
			UGenotypeFC* offspring2;
			completeCrossoverFC((UGenotypeFC*)intermediatePopulation[0], (UGenotypeFC*)intermediatePopulation[1], defCrossSwapProb, offspring1, offspring2);
			newPopulation.Add(offspring1);
			if (newPopulation.Num() < newPopulationSize) newPopulation.Add(offspring2);
		}
		else {
			UGenotypeNEAT* offspring;
			completeCrossoverNEAT((UGenotypeNEAT*) intermediatePopulation[0], (UGenotypeNEAT*) intermediatePopulation[1], defCrossSwapProb, offspring);
			newPopulation.Add(offspring);
		}

	}

	return newPopulation;
}

TArray<UGenotype*> UGeneticAlgorithm::randomRecombination(TArray<UGenotype*>& intermediatePopulation, int newPopulationSize) {

	check(intermediatePopulation.Num() >= 2);

	TArray<UGenotype*> newPopulation;
	// Always add best two
	newPopulation.Add(intermediatePopulation[0]);
	newPopulation.Add(intermediatePopulation[1]);

	while (newPopulation.Num() < newPopulationSize - defRandomCount) {
		// Generate two different random indeces
		int ind1 = FMath::RandRange(0, intermediatePopulation.Num() - 1);
		int ind2;
		do {
			ind2 = FMath::RandRange(0, intermediatePopulation.Num() - 1);
		} while (ind1 == ind2);

		if (!neat) {
			UGenotypeFC* offspring1;
			UGenotypeFC* offspring2;
			completeCrossoverFC((UGenotypeFC*)intermediatePopulation[ind1], (UGenotypeFC*)intermediatePopulation[ind2], defCrossSwapProb, offspring1, offspring2);
			newPopulation.Add(offspring1);
			if (newPopulation.Num() < newPopulationSize) newPopulation.Add(offspring2);
		}
		else {
			UGenotypeNEAT* offspring;
			completeCrossoverNEAT((UGenotypeNEAT*)intermediatePopulation[ind1], (UGenotypeNEAT*)intermediatePopulation[ind2], defCrossSwapProb, offspring);
			newPopulation.Add(offspring);
		}
	}
	for (int i = 0; i < defRandomCount; ++i) {
		if (!neat) {
			UGenotypeFC* genotype = NewObject<UGenotypeFC>();
			genotype->Init(((UGenotypeFC*) intermediatePopulation[0])->layersSizes, ((UGenotypeFC*)intermediatePopulation[0])->layersFuncs, defInitParamMin, defInitParamMax);
			genotype->AddToRoot();
			newPopulation.Add(genotype);
		}
		/*else {
			UGenotypeNEAT* genotype = NewObject<UGenotypeNEAT>();
			genotype->Init((UGenotypeNEAT*) intermediatePopulation[0]);
			genotype->randomizeGenotype(defInitParamMin, defInitParamMax);
			genotype->AddToRoot();
			newPopulation.Add(genotype);
		}*/
	}

	return newPopulation;
}

// Mutate all members of population with default parameters
void UGeneticAlgorithm::defaultMutationOperator(TArray<UGenotype*>& newPopulation) {
	
	for (UGenotype* genotype : newPopulation) {
		if (FMath::FRandRange(0, 1) < defMutationPart) {
			if (!neat) mutateGenotypeFC((UGenotypeFC*) genotype, defMutationProb, defMutationAmount);
			else mutateGenotypeNEAT((UGenotypeNEAT*) genotype, defAddNodeProb, defAddLinkProb, defMutateWeightsProb, defToggleEnableProb);
		}
	}

}

// Mutate all members of population with default parameters, leaving the first 2 genotypes untouched
void UGeneticAlgorithm::mutateAllButBestTwo(TArray<UGenotype*>& newPopulation) {

	for (int i = 2; i < newPopulation.Num(); ++i) {
		if (FMath::FRandRange(0, 1) < defMutationPart) {
			if (!neat) mutateGenotypeFC((UGenotypeFC*) newPopulation[i], defMutationProb, defMutationAmount);
			else mutateGenotypeNEAT((UGenotypeNEAT*) newPopulation[i], defAddNodeProb, defAddLinkProb, defMutateWeightsProb, defToggleEnableProb);
		}
	}

}

void UGeneticAlgorithm::completeCrossoverFC(UGenotypeFC* parent1, UGenotypeFC* parent2, float swapChance, UGenotypeFC*& offspring1, UGenotypeFC*& offspring2) {

	// initialize new parameter vectors
	int parametersCount = parent1->parametersCount;
	float* off1Parameters = new float[parametersCount];
	float* off2Parameters = new float[parametersCount];

	for (int i = 0; i < parametersCount; ++i) {
		if (FMath::FRandRange(0, 1) < swapChance) {
			// Swap parameters
			off1Parameters[i] = parent2->parameters[i];
			off2Parameters[i] = parent1->parameters[i];
		}
		else {
			// Don't swap parameters
			off1Parameters[i] = parent1->parameters[i];
			off2Parameters[i] = parent2->parameters[i];
		}
	}

	offspring1 = NewObject<UGenotypeFC>();
	offspring1->Init(parent1->layersSizes, parent1->layersFuncs, off1Parameters);
	offspring1->AddToRoot();

	offspring2 = NewObject<UGenotypeFC>();
	offspring2->Init(parent1->layersSizes, parent1->layersFuncs, off2Parameters);
	offspring2->AddToRoot();
}

void UGeneticAlgorithm::completeCrossoverNEAT(UGenotypeNEAT* parent1, UGenotypeNEAT* parent2, float swapChance, UGenotypeNEAT*& offspring) {
	parent1->linkGenes.Sort();
	parent2->linkGenes.Sort();

	if (parent2 > parent1) {
		UGenotypeNEAT* t = parent1;
		parent1 = parent2;
		parent2 = parent1;
	}
	
	offspring = NewObject<UGenotypeNEAT>();
	offspring->Init(parent1);
	offspring->AddToRoot();

	// Two pointers method
	// i - parent1 (offspring)
	// j - parent2
	int j = 0;
	for (int i = 0; i < offspring->linkGenes.Num(); ++i) {

		while (j < parent2->linkGenes.Num() && parent2->linkGenes[j].innovationNumber < offspring->linkGenes[i].innovationNumber) j++;
		if (j >= parent2->linkGenes.Num()) break;
		if (parent2->linkGenes[j].innovationNumber == offspring->linkGenes[i].innovationNumber) {
			if (FMath::FRandRange(0, 1) < swapChance) offspring->linkGenes[i] = parent2->linkGenes[j];

			//If one is disabled, the corresponding gene in the offspring
			//will likely be disabled
			if (!parent1->linkGenes[i].enabled || !parent2->linkGenes[j].enabled)
				if (FMath::FRandRange(0, 1) < 0.75) offspring->linkGenes[i].enabled = false;
				else offspring->linkGenes[i].enabled = true;
			++j;
		}

	}
}

void UGeneticAlgorithm::mutateGenotypeFC(UGenotypeFC* genotype, float mutationProb, float mutationAmount)
{
	if (genotype == NULL) return;
	for (int i = 0; i < genotype->parametersCount; ++i)
	{
		if (FMath::FRand() < mutationProb)
		{
			// mutate by random amount in range [-mutatonAmount; mutationAmount]
			genotype->parameters[i] += FMath::FRandRange(-mutationAmount, mutationAmount);
		}
	}
}

void UGeneticAlgorithm::mutateGenotypeNEAT(UGenotypeNEAT* genotype, float addNodeProb, float addLinkProb, float mutateWeightsProb, float toggleEnableProb) {
	if (FMath::FRandRange(0, 1) < addNodeProb) mutateAddNode(genotype, innovations);
	else if (FMath::FRandRange(0, 1) < addLinkProb) mutateAddLink(genotype, innovations);
	else {
		if (FMath::FRandRange(0, 1) < mutateWeightsProb) mutateLinkWeights(genotype, 1.0, 1.0);
		if (FMath::FRandRange(0, 1) < toggleEnableProb) mutateToggleEnable(genotype, 1);
	}
}

void UGeneticAlgorithm::mutateLinkWeights(UGenotypeNEAT* genotype, float power, float rate) {
	bool severe = false;
	if (FMath::FRandRange(0, 1) > 0.5) severe = true;

	float gausspoint, coldgausspoint;
	for (int i = 0; i < genotype->linkGenes.Num(); ++i) {
		if (severe) {
			gausspoint = 0.3;
			coldgausspoint = 0.1;
		}
		else if((genotype->linkGenes.Num() >= 10) && (i > genotype->linkGenes.Num() * 0.8)) {
			gausspoint = 0.5;
			coldgausspoint = 0.3;
		} else {
			gausspoint = 1.0 - rate;
			coldgausspoint = 1.0 - rate;
			if (FMath::FRandRange(0, 1) < 0.5) coldgausspoint -= 0.1;
		}

		float mutationAmount = FMath::FRandRange(-1.0, 1.0) * power;
		float rand = FMath::FRandRange(0, 1);
		if (rand > gausspoint) {
			genotype->linkGenes[i].weight += mutationAmount;
		}
		else if(rand > coldgausspoint) {
			genotype->linkGenes[i].weight = mutationAmount;
		}
	}
}

void UGeneticAlgorithm::mutateToggleEnable(UGenotypeNEAT* genotype, int times) {
	for (int i = 1; i <= times; ++i) {
		LinkGene& gene = genotype->linkGenes[FMath::RandRange(0, genotype->linkGenes.Num() - 1)];

		if (gene.enabled) {
			int j = 0;
			for (; (j < genotype->linkGenes.Num()) && ((genotype->linkGenes[j].inputInnov != gene.inputInnov) || (!genotype->linkGenes[j].enabled) || (genotype->linkGenes[j].innovationNumber == gene.innovationNumber)); ++j);
			if (j >= genotype->linkGenes.Num()) gene.enabled = false;
		}
		else gene.enabled = true;
	}
}

bool UGeneticAlgorithm::mutateAddNode(UGenotypeNEAT* genotype, TArray<Innovation>& innovations) {
	int attemptsCount = 0;
	int geneNumber;
	for (; (attemptsCount < 20) && !(genotype->linkGenes[geneNumber = FMath::RandRange(0, genotype->linkGenes.Num() - 1)].enabled && innovations[genotype->linkGenes[geneNumber].inputInnov].nodeType != NodeType::BIAS); ++attemptsCount);
	if (attemptsCount >= 20) return false;

	LinkGene& gene = genotype->linkGenes[geneNumber];
	gene.enabled = false;

	int innovationNumber = innovations.IndexOfByPredicate([gene](const Innovation& innovation) {
		return innovation.type == InnovationType::NODE && innovation.replacedLinkInnov == gene.innovationNumber;
	});

	if (innovationNumber == INDEX_NONE) {
		genotype->nodeGenes.Add(NodeGene(NodeType::HIDDEN, innovations.Num()));
		innovations.Add(Innovation(NodeType::HIDDEN, gene.innovationNumber));

		genotype->linkGenes.Add(LinkGene(gene.inputInnov, innovations.Num() - 1, 1.0, innovations.Num()));
		innovations.Add(Innovation(genotype->linkGenes.Last()));

		genotype->linkGenes.Add(LinkGene(innovations.Num() - 2, gene.outputInnov, gene.weight, innovations.Num()));
		innovations.Add(Innovation(genotype->linkGenes.Last()));
	}
	else {
		int index = genotype->nodeGenes.IndexOfByPredicate([innovationNumber](const NodeGene& gene) {
			return gene.innovationNumber == innovationNumber;
		});
		if (index == INDEX_NONE) {
			genotype->nodeGenes.Add(NodeGene(NodeType::HIDDEN, innovationNumber));

			genotype->linkGenes.Add(LinkGene(gene.inputInnov, innovationNumber, 1.0, innovationNumber + 1));

			genotype->linkGenes.Add(LinkGene(innovationNumber, gene.outputInnov, gene.weight, innovationNumber + 2));
		}
	}

	return true;
}

bool UGeneticAlgorithm::mutateAddLink(UGenotypeNEAT* genotype, TArray<Innovation>& innovations) {
	int i = 0;
	int node1, node2, innov1, innov2;
	for (; i < 20; ++i) {
		do{
			node1 = FMath::RandRange(0, genotype->nodeGenes.Num() - 1);
		} while (genotype->nodeGenes[node1].type == NodeType::OUTPUT);
		do {
			node2 = FMath::RandRange(0, genotype->nodeGenes.Num() - 1);
		} while (genotype->nodeGenes[node2].type == NodeType::INPUT || genotype->nodeGenes[node2].type == NodeType::BIAS);
		innov1 = genotype->nodeGenes[node1].innovationNumber;
		innov2 = genotype->nodeGenes[node2].innovationNumber;
		int index = genotype->linkGenes.IndexOfByPredicate([innov1, innov2](const LinkGene& link) {
			return (link.inputInnov == innov1) && (link.outputInnov == innov2);
		});
		if (index != INDEX_NONE) continue; // if found such link then trying to find new
		if (is_recurrent(genotype, node1, node2)) continue; // link can't be recurrent
		break;
	}
	if (i >= 20) return false;

	int innovationNumber = innovations.IndexOfByPredicate([innov1, innov2](const Innovation& innovation) {
		return (innovation.type == InnovationType::LINK) && (innovation.inputInnov == innov1) && (innovation.outputInnov == innov2);
	});
	if (innovationNumber == INDEX_NONE) {
		genotype->linkGenes.Add(LinkGene(innov1, innov2, FMath::FRandRange(-1, 1), innovations.Num()));
		innovations.Add(Innovation(genotype->linkGenes.Last()));
	}
	else {
		genotype->linkGenes.Add(LinkGene(innov1, innov2, FMath::FRandRange(-1, 1), innovationNumber));
	}

	return true;
}

bool UGeneticAlgorithm::is_recurrent(UGenotypeNEAT* genotype, int node1, int node2) {
	
	TSortedMap< int, TArray< int > > inputLinks;

	for (int i = 0; i < genotype->linkGenes.Num(); ++i) {
		inputLinks.FindOrAdd(genotype->linkGenes[i].outputInnov).Add(genotype->linkGenes[i].inputInnov);
	}

	return dfs(genotype->nodeGenes[node1].innovationNumber, inputLinks, genotype->nodeGenes[node2].innovationNumber);
}

bool UGeneticAlgorithm::dfs(int u, TSortedMap< int, TArray< int > >& inputLinks, int v) {
	if (u == v) return true;

	TArray<int> links = inputLinks.FindOrAdd(u);
	
	for (int i = 0; i < links.Num(); ++i) {
		if (dfs(links[i], inputLinks, v)) return true;
	}
	
	return false;
}

void UGeneticAlgorithm::speciate(TArray<UGenotype*>& l_currentPopulation) {
	if (((UGenotypeNEAT*)l_currentPopulation[0]) == NULL) return;

	int speciesCount = species.Num();
	UGenotypeNEAT* bestGenotype;
	for (int i = 0; i < species.Num(); ++i) {
		bestGenotype = species[i].population[0];
		for (int j = 0; j < species[i].population.Num(); ++j)
			if (species[i].population[j]->evaluation > bestGenotype->evaluation)
				bestGenotype = species[i].population[j];
		species[i].population.Empty();
		species[i].population.Add(bestGenotype);
	}

	UGenotypeNEAT* genotype;
	for (int i = 0; i < l_currentPopulation.Num(); ++i) {
		genotype = (UGenotypeNEAT*)l_currentPopulation[i];
		int j = 0;
		for (; j < species.Num() && compatibilityNEAT(genotype, species[j].population[0], defExcessCoeff, defDisjointCoeff, defWeightsDiffCoeff) >= compatibilityThreshold; ++j);
		if (j >= species.Num()) {
			species.Add(Species());
			species.Last().population.Add(genotype);
		}
		else {
			species[j].population.Add(genotype);
		}
	}

	for (int i = 0; i < speciesCount; ++i) species[i].population.RemoveAt(0);
	species.RemoveAll([](const Species& spec) {
		return spec.population.Num() <= 0;
	});
	UE_LOG(LogTemp, Warning, TEXT("Species:"));
	for (Species& spec : species)
		UE_LOG(LogTemp, Warning, TEXT("Num: %d"), spec.population.Num());
}

float UGeneticAlgorithm::compatibilityNEAT(UGenotypeNEAT* genotype1, UGenotypeNEAT* genotype2, float excessCoeff, float disjointCoeff, float weightsDiffCoeff) {
	
	genotype1->linkGenes.Sort();
	genotype2->linkGenes.Sort();
	
	float weightsDiff = 0.0;
	int matchingCount = 0;

	int i = 0, j = 0;
	for (; i < genotype1->linkGenes.Num() && j < genotype2->linkGenes.Num(); ++i) {
		while (j < genotype2->linkGenes.Num() && genotype2->linkGenes[j].innovationNumber < genotype1->linkGenes[i].innovationNumber) j++;
		if (j == genotype2->linkGenes.Num()) break;
		if (genotype2->linkGenes[j].innovationNumber == genotype1->linkGenes[i].innovationNumber) {
			matchingCount++;
			weightsDiff += abs(genotype1->linkGenes[i].weight - genotype2->linkGenes[j].weight);
		}
	}
	int excessCount = FMath::Max(genotype1->linkGenes.Num() - i, genotype2->linkGenes.Num() - j);
	int disjointCount = FMath::Min(genotype1->linkGenes.Num(), genotype2->linkGenes.Num()) - matchingCount;

	float result = (excessCoeff * excessCount + disjointCoeff * disjointCount) / FMath::Max(genotype1->linkGenes.Num(), genotype2->linkGenes.Num()) + weightsDiffCoeff * weightsDiff / matchingCount;
	UE_LOG(LogTemp, Warning, TEXT("Compatibility result: %f, matchingCount: %d, excessCount: %d, disjointCount: %d"), result, matchingCount, excessCount, disjointCount);
	return result;
}
