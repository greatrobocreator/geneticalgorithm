#include "NeuralLayer.h"


NeuralLayer::NeuralLayer(size_t l_size, size_t l_inputSize, EActivationFunc l_activationFunc) : size(l_size), inputSize(l_inputSize), activationFunc(l_activationFunc) {
	
	//outputLayer = false;
	weights = new float*[size];
	for (int i = 0; i < size; ++i) {
		weights[i] = new float[inputSize + 1];
		for (int j = 0; j <= inputSize; ++j) weights[i][j] = FMath::FRandRange(-2, 2);
	}

	//printWeights();

}

void NeuralLayer::printWeights() {

	UE_LOG(LogTemp, Warning, TEXT("Layer with size: %d, inputSize: %d"), size, inputSize);
	for (int i = 0; i <= size; ++i) {
		for (int j = 0; j < inputSize; ++j) {
			UE_LOG(LogTemp, Warning, TEXT("weights[%d][%d] = %f"), i, j, weights[i][j]);
		}
	}
}

NeuralLayer::NeuralLayer(size_t l_size, size_t l_inputSize, float** l_weights, EActivationFunc l_activationFunc) : NeuralLayer(l_size, l_inputSize, l_activationFunc) {
	
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j <= inputSize; ++j) {
			weights[i][j] = l_weights[i][j];
		}
	}
}

NeuralLayer::NeuralLayer(size_t l_size, size_t l_inputSize, float* l_weights, EActivationFunc l_activationFunc) : NeuralLayer(l_size, l_inputSize, l_activationFunc) {

	// reading input from parameters array
	// first for the first neuron
	// then for then second etc.

	for (int i = 0; i < size; ++i) {
		for (int j = 0; j <= inputSize; ++j) {
			weights[i][j] = l_weights[i * (inputSize + 1) + j];
		}
	}
}

/*NeuralLayer::NeuralLayer(size_t l_size, EActivationFunc l_activationFunc) : NeuralLayer(l_size, l_size, l_activationFunc) {
	outputLayer = true;
}*/

/*NeuralLayer::NeuralLayer(size_t l_size, float** l_weights, EActivationFunc l_activationFunc) : NeuralLayer(l_size, l_size, l_weights, l_activationFunc) {
	outputLayer = true;
}*/

NeuralLayer::NeuralLayer() : NeuralLayer(0, 0, EActivationFunc::Linear) {}

float NeuralLayer::ActivateNeuron(float input) {
	switch (activationFunc) {
	case EActivationFunc::Linear:
		return input;
		break;

	case EActivationFunc::ReLu:
		return (input < 0 ? 0 : input);
		break;

	case EActivationFunc::PReLu:
		return (input <= 0 ? (input / 10) : input);
		break;

	case EActivationFunc::Sigmoid:
		return 1 / (1 + exp(-input));
		break;

	case EActivationFunc::Tanh:
		return (exp(2 * input) - 1) / (exp(2 * input) + 1);
		break;
	}
	return input;
}

float* NeuralLayer::Process(float* input) {

	float* output = new float[size];


	/*if (!outputLayer) {

		for (int i = 0; i < inputSize; ++i) output[i] = 0;

		for (int i = 0; i <= size; ++i) {
			int neuronOutput = (i < size ? ActivateNeuron(input[i]) : 1);
			for (int j = 0; j < inputSize; ++j) {
				output[j] += neuronOutput * weights[i][j];
			}
		}
	}
	else {
		for (int i = 0; i < size; ++i) {
			output[i] = ActivateNeuron(input[i]);
		}
	}*/

	for (int i = 0; i < size; ++i) output[i] = 0;

	for (int i = 0; i <= inputSize; ++i) {
		float neuronValue = (i < inputSize ? input[i] : 1); // for bias
		for (int j = 0; j < size; ++j) {
			output[j] += neuronValue * weights[j][i];
		}
	}

	for (int i = 0; i < size; ++i) output[i] = ActivateNeuron(output[i]);

	return output;

}

/*NeuralLayer::NeuralLayer()
{
}*/


NeuralLayer::~NeuralLayer() {
	/*if (weights)
		delete[] weights;*///for (int i = 0; i <= size; ++i) delete[] weights[i];
}
