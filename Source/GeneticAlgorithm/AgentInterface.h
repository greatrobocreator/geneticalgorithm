// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "GenotypeFC.h"
#include "NEAT/GenotypeNEAT.h"
#include "NeuralLayer.h"

#include "AgentInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI, Blueprintable)
class UAgentInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class GENETICALGORITHM_API IAgentInterface
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)//, meta = (AutoCreateTermRef = "layersSizes, layersFuncs"))
	void Init(UGenotype* genotype);

	UFUNCTION(BLueprintCallable, BlueprintImplementableEvent)
	float getEvaluation();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void getGenotype(TArray<float>& parameters, TArray<int>& layersSizes, TArray< EActivationFunc >& layersFuncs);

};
