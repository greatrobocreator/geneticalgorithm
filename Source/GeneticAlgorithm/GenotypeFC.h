// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Math/UnrealMathUtility.h"

#include "Genotype.h"

#include "GenotypeFC.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class GENETICALGORITHM_API UGenotypeFC : public UGenotype
{
	GENERATED_BODY()

public:
	// variables
	int parametersCount;
	float* parameters;
	TArray<int> layersSizes;
	TArray<EActivationFunc> layersFuncs;

	// constructors & destructors
	UGenotypeFC();
	UGenotypeFC(TArray<int>& l_layersSizes, TArray<EActivationFunc>& l_layersFuncs, float minValue = -1, float maxValue = 1, int additionalParameters = 0);
	//UGenotypeFC(int parametersCount, float* l_parameters);
	// from other genotype
	UGenotypeFC(UGenotypeFC* genotype);
	UGenotypeFC(UGenotype* genotype) : UGenotypeFC((UGenotypeFC*) genotype) {};
	~UGenotypeFC();

	// Random initialization
	void Init(TArray<int>& l_layersSizes, TArray<EActivationFunc>& l_layersFuncs, float minValue = -1, float maxValue = 1, int additionalParameters = 0);
	 // Initialize by variables
	void Init(TArray<int>& l_layersSizes, TArray<EActivationFunc>& l_layersFuncs, float* l_parameters, int additionalParameters = 0);
	// Initialize from other genotype (copy all variables)
	void Init(UGenotypeFC* genotype);

	// Initialize from TArray (for blueprints)
	UFUNCTION(BlueprintCallable, meta = (AutoCreateRefTerm = "l_parameters, l_layersSizes, l_layersFuncs"))
	void Init(const TArray<float>& l_parameters, const TArray<int>& l_layersSizes, const TArray<EActivationFunc>& l_layersFuncs);

	/* // operators
	bool operator < (const UGenotypeFC& r) const;*/

	// functions
	void randomizeGenotype(float minValue, float maxValue);
	float* makeParametersCopy();

	// getters
	// return parameters converted to TArray
	UFUNCTION(BlueprintCallable, BlueprintPure)
	TArray<float> getParameters();

private:
	int countWeights(TArray<int>& layersSizes);

};
