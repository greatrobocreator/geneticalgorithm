// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "Engine/EngineTypes.h"

#include "AgentInterface.h"

#include "Agent.generated.h"

DECLARE_EVENT_OneParam(AAgent, FAgentDied, AActor*)

UCLASS()
class GENETICALGORITHM_API AAgent : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAgent();

	void Init(UClass* l_subjectClass, UGenotype* l_genotype, FVector location, FRotator rotation);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//variables
	UPROPERTY(BlueprintReadOnly)
	UGenotype* genotype;
	UClass* subjectClass;
	AActor* subject;

	float lastSubjectEvaluation = 0.0f;

	FAgentDied agentDied;
	FTimerHandle evaluationCheckHandle;

	//funcitons
	UFUNCTION(BlueprintCallable)
	void OnSubjectDied(AActor* actor, EEndPlayReason::Type reason);

	void checkSubjectEvaluation();

};
