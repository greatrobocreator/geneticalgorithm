// Fill out your copyright notice in the Description page of Project Settings.


#include "Genotype.h"


UGenotype::UGenotype(UGenotype* genotype) {
	Init(genotype);
}

void UGenotype::Init(UGenotype* genotype) {
	if (genotype != NULL) {
		this->fitness = genotype->fitness;
		this->evaluation = genotype->evaluation;
	}
}

