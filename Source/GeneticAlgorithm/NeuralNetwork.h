// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "NeuralLayer.h"
#include "GenotypeFC.h"

#include "NeuralNetwork.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class GENETICALGORITHM_API UNeuralNetwork : public UActorComponent
{
	GENERATED_BODY()

public:
	// Constructors
	// Sets default values for this component's properties
	UNeuralNetwork();
	UNeuralNetwork(TArray< int >& l_layersSizes, TArray< EActivationFunc >& l_layersFuncs);

	void Init();

	UFUNCTION(BlueprintCallable)//, meta = (AutoCreateTermRef = "l_layersSizes, l_layersFuncs"))
	void Init(UGenotypeFC* genotype);//, const TArray<int>& l_layersSizes, const TArray<EActivationFunc>& l_layersFuncs);

	// functions
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void printLayersWeights();

	UFUNCTION(BlueprintCallable, meta = (AutoCreateRefTerm = "input"))
	TArray<float> Process(const TArray<float>& input);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	TArray<float> getParameters();

	//variables
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray< int > layersSizes;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray< EActivationFunc > layersFuncs;

private:

	//TArray<NeuralLayer> layers;
	NeuralLayer* layers;
	//FTimerHandle WeightsTimerHandle;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

};
