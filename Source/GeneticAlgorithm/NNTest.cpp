// Fill out your copyright notice in the Description page of Project Settings.


#include "NNTest.h"

// Sets default values
ANNTest::ANNTest()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ANNTest::BeginPlay()
{
	TArray<Innovation> innovations;
	genotype = NewObject<UGenotypeNEAT>();
	genotype->Init(5, 2, innovations);
	UGeneticAlgorithm::mutateAddNode(genotype, innovations);
	UGeneticAlgorithm::mutateAddNode(genotype, innovations);
	UGeneticAlgorithm::mutateAddNode(genotype, innovations);
	/*UGeneticAlgorithm::mutateAddLink(genotype, innovations);
	UGeneticAlgorithm::mutateAddLink(genotype, innovations);
	UGeneticAlgorithm::mutateAddLink(genotype, innovations);*/
	for (int i = 0; i < 100; ++i) {
		int node1 = FMath::RandRange(0, genotype->nodeGenes.Num() - 1);
		int node2 = FMath::RandRange(0, genotype->nodeGenes.Num() - 1);
		UE_LOG(LogTemp, Warning, TEXT("%d -> %d: %s"), node1, node2, (UGeneticAlgorithm::is_recurrent(genotype, node1, node2) ? TEXT("True") : TEXT("False")));
	}
	/*genotype->nodeGenes.Add(NodeGene(NodeType::INPUT, 0));
	genotype->nodeGenes.Add(NodeGene(NodeType::INPUT, 1));
	genotype->nodeGenes.Add(NodeGene(NodeType::OUTPUT, 2));
	genotype->nodeGenes.Add(NodeGene(NodeType::HIDDEN, 3));
	genotype->nodeGenes.Add(NodeGene(NodeType::HIDDEN, 4));
	genotype->linkGenes.Add(LinkGene(0, 3, 0.45, 5));
	genotype->linkGenes.Add(LinkGene(0, 4, 0.78, 6));
	genotype->linkGenes.Add(LinkGene(1, 3, -0.12, 7));
	genotype->linkGenes.Add(LinkGene(1, 4, 0.13, 8));
	genotype->linkGenes.Add(LinkGene(3, 2, 1.5, 9));
	genotype->linkGenes.Add(LinkGene(4, 2, -2.3, 10));*/

	Super::BeginPlay();
	
}

// Called every frame
void ANNTest::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
