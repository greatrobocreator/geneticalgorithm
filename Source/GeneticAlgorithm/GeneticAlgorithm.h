// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Containers/Array.h"
#include "Containers/Map.h"
#include "Containers/SortedMap.h"
#include "Containers/UnrealString.h"

#include "GenotypeFC.h"
#include "NEAT/GenotypeNEAT.h"
#include "NEAT/Species.h"

#include "GeneticAlgorithm.generated.h"

DECLARE_DYNAMIC_DELEGATE_OneParam(FInitializationOperator, TArray<UGenotype*>&, currentPopulation);
DECLARE_DYNAMIC_DELEGATE_OneParam(FEvaluationOperator, TArray<UGenotype*>&, currentPopulation);
DECLARE_DYNAMIC_DELEGATE_OneParam(FFitnessCalculation, TArray<UGenotype*>&, currentPopulation);
DECLARE_DYNAMIC_DELEGATE_RetVal_OneParam(TArray<UGenotype*>, FSelectionOperator, TArray<UGenotype*>&, currentPopulation);
DECLARE_DYNAMIC_DELEGATE_RetVal_TwoParams(TArray<UGenotype*>, FRecombinationOperator, TArray<UGenotype*>&, intermediatePopulation, int, newPopulationSize);
DECLARE_DYNAMIC_DELEGATE_OneParam(FMutationOperator, TArray<UGenotype*>&, newPopulation);
DECLARE_DYNAMIC_DELEGATE_RetVal_OneParam(bool, FCheckTerminationCriterion, TArray<UGenotype*>&, currentPopulation);

DECLARE_EVENT_OneParam(UGeneticAlgorithm, FAlgorithmTerminated, UGeneticAlgorithm*)

UCLASS(Blueprintable)
class GENETICALGORITHM_API UGeneticAlgorithm : public UObject
{
	GENERATED_BODY()
	
public:

	// variables
	float defInitParamMin = -1.0f;
	float defInitParamMax = 1.0f;
	float defCrossSwapProb = 0.5f;
	// Mutation probabilities
	float defMutationProb = 0.1f;
	float defMutationAmount = 2.0f;
	float defMutationPart = 1.0f;
	float defAddNodeProb = 0.03;
	float defAddLinkProb = 0.05;
	float defMutateWeightsProb = 0.9;
	float defToggleEnableProb = 0.0;

	int defRandomCount = 5;

	int dropoffAge = 15; // Age when species begin to be penalized
	float youngBonus = 1.0;

	// Compatibility coefficients
	float defDisjointCoeff = 1.0;
	float defExcessCoeff = 1.0;
	float defWeightsDiffCoeff = 2.0;
	float compatibilityThreshold = 3.0;

	TArray<UGenotype*> currentPopulation;
	TArray<Innovation> innovations;
	TArray<Species> species;

	int populationSize;

	UPROPERTY(BlueprintReadOnly)
	int generationCount;

	bool sortPopulation;
	bool running;
	bool neat;

	// constructors
	UGeneticAlgorithm();
	//UGeneticAlgorithm(TArray<int>& l_layersSizes, TArray<EActivationFunc>& l_layersFuncs, int l_populationSize);

	// functions
	void Init(TArray<int>& l_layersSizes, TArray<EActivationFunc>& l_layersFuncs, int l_populationSize, bool l_neat = false);
	void start();
	void evaluationFinished();

	// operators
	UFUNCTION(BlueprintCallable)
	void defaultPopulationInitialization(TArray<UGenotype*>& population);

	UFUNCTION(BlueprintCallable)
	void defaultFitnessCalculation(TArray<UGenotype*>& l_currentPopulation);

	UFUNCTION(BlueprintCallable)
	TArray<UGenotype*> defaultSelectionOperator(TArray<UGenotype*>& l_currentPopulation);

	UFUNCTION(BlueprintCallable)
	TArray<UGenotype*> remainderStochasticSampling(TArray<UGenotype*>& l_currentPopulation);

	UFUNCTION(BlueprintCallable)
	TArray<UGenotype*> defaultRecombinationOperator(TArray<UGenotype*>& intermediatePopulation, int newPopulationSize);

	UFUNCTION(BlueprintCallable)
	TArray<UGenotype*> randomRecombination(TArray<UGenotype*>& intermediatePopulation, int newPopulationSize);

	UFUNCTION(BlueprintCallable)
	void defaultMutationOperator(TArray<UGenotype*>& newPopulation);

	UFUNCTION(BlueprintCallable)
	void mutateAllButBestTwo(TArray<UGenotype*>& newPopulation);

	// recombination operators
	void completeCrossoverFC(UGenotypeFC* parent1, UGenotypeFC* parent2, float swapChance, UGenotypeFC*& offspring1, UGenotypeFC*& offspring2);
	void completeCrossoverNEAT(UGenotypeNEAT* parent1, UGenotypeNEAT* parent2, float swapChance, UGenotypeNEAT*& offsping);

	// mutation operators
	void mutateGenotypeFC(UGenotypeFC* genotype, float mutationProb, float mutationAmount);
	void mutateGenotypeNEAT(UGenotypeNEAT* genotype, float addNodeProb, float addLinkProb, float mutateWeightsProb, float toggleEnableProb);

	// operators' functions
	FInitializationOperator initializePopulation;
	FEvaluationOperator evaluation;
	FFitnessCalculation fitnessCalculation;
	FSelectionOperator selection;
	FRecombinationOperator recombination;
	FMutationOperator mutation;
	FCheckTerminationCriterion terminationCriterion;

	// events
	FAlgorithmTerminated AlgorithmTerminated;

private:
	void terminate();

	// u - current vertex
	// v - target vertex
	static bool dfs(int u, TSortedMap< int, TArray< int > >& inputLinks, int v);

	// Divide population by species
	void speciate(TArray<UGenotype*>& l_currentPopulation);

public:
	// check if link from node1 to node2 is recurrent
	static bool is_recurrent(UGenotypeNEAT* genotype, int node1, int node2);

	// NEAT Genotype mutations
	static void mutateLinkWeights(UGenotypeNEAT* genotype, float power, float rate);
	static void mutateToggleEnable(UGenotypeNEAT* genotype, int times);
	static bool mutateAddNode(UGenotypeNEAT* genotype, TArray<Innovation>& innovations);
	static bool mutateAddLink(UGenotypeNEAT* genotype, TArray<Innovation>& innovations);

	static float compatibilityNEAT(UGenotypeNEAT* genotype1, UGenotypeNEAT* genotype2, float excessCoeff, float disjointCoeff, float weightsDiffCoeff);
};
