// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "ActivationFunc.h"

#include "Genotype.generated.h"


UCLASS(Blueprintable)
class GENETICALGORITHM_API UGenotype : public UObject
{
	GENERATED_BODY()
	
public:
	// variables
	float fitness;
	float evaluation;

	// constructors & destructors
	UGenotype() : fitness(0), evaluation(0) {};
	UGenotype(UGenotype* genotype);
	~UGenotype() {};

	// Initialize from other genotype
	virtual void Init(UGenotype* genotype);

	// operators
	virtual bool operator < (const UGenotype& r) const { return fitness > r.fitness; };

	// functions
	virtual void randomizeGenotype(float minValue, float maxValue) {};

};
