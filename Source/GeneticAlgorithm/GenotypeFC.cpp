#include "GenotypeFC.h"

UGenotypeFC::UGenotypeFC() {};

UGenotypeFC::UGenotypeFC(TArray<int>& l_layersSizes, TArray<EActivationFunc>& l_layersFuncs, float minValue, float maxValue, int additionalParameters) {
	Init(l_layersSizes, l_layersFuncs, minValue, maxValue, additionalParameters);
}

/*UGenotypeFC::UGenotypeFC(int l_parametersCount, float* l_parameters) {
	Init(l_parametersCount, l_parameters);
}*/

UGenotypeFC::UGenotypeFC(UGenotypeFC* genotype) : UGenotype(genotype) {
	Init(genotype);
}

void UGenotypeFC::Init(TArray<int>& l_layersSizes, TArray<EActivationFunc>& l_layersFuncs, float minValue, float maxValue, int additionalParameters) {

	layersSizes = l_layersSizes;
	layersFuncs = l_layersFuncs;
	parametersCount = countWeights(layersSizes) + additionalParameters;
	parameters = new float[parametersCount];
	randomizeGenotype(minValue, maxValue);

}

void UGenotypeFC::Init(TArray<int>& l_layersSizes, TArray<EActivationFunc>& l_layersFuncs, float* l_parameters, int additionalParameters) {
	
	layersSizes = l_layersSizes;
	layersFuncs = l_layersFuncs;
	parametersCount = countWeights(layersSizes) + additionalParameters;
	parameters = new float[parametersCount];
	for (int i = 0; i < parametersCount; ++i) {
		parameters[i] = l_parameters[i];
	}
}

void UGenotypeFC::Init(UGenotypeFC* genotype) {
	if (genotype != NULL) {
		Init(genotype->layersSizes, genotype->layersFuncs, genotype->parameters);
	}
}

void UGenotypeFC::Init(const TArray<float>& l_parameters, const TArray<int>& l_layersSizes, const TArray<EActivationFunc>& l_layersFuncs) {
	layersSizes = l_layersSizes;
	layersFuncs = l_layersFuncs;
	parametersCount = l_parameters.Num();
	parameters = new float[parametersCount];
	for (int i = 0; i < parametersCount; ++i) {
		parameters[i] = l_parameters[i];
	}
}

UGenotypeFC::~UGenotypeFC()
{
	layersSizes.Empty();
	layersFuncs.Empty();
	delete[] parameters;
}

//bool UGenotypeFC::operator < (const UGenotypeFC& r) const { return fitness > r.fitness; }

void UGenotypeFC::randomizeGenotype(float minValue, float maxValue) {

	for (int i = 0; i < parametersCount; ++i) parameters[i] = FMath::FRandRange(minValue, maxValue);

}

float* UGenotypeFC::makeParametersCopy() {

	float* copy = new float[parametersCount];
	for (int i = 0; i < parametersCount; ++i) {
		copy[i] = parameters[i];
	}
	return copy;
}

TArray<float> UGenotypeFC::getParameters() {

	TArray<float> output;

	for (int i = 0; i < parametersCount; ++i) {
		output.Add(parameters[i]);
	}

	return output;

}

int UGenotypeFC::countWeights(TArray<int>& l_layersSizes) {
	// Calculating NN weights count
	int weightsCount = 0;
	for (int i = 1; i < l_layersSizes.Num(); ++i) weightsCount += l_layersSizes[i] * (l_layersSizes[i - 1] + 1); // + 1 for bias

	return weightsCount;
}