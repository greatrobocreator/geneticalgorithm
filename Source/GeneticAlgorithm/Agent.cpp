// Fill out your copyright notice in the Description page of Project Settings.


#include "Agent.h"

// Sets default values
AAgent::AAgent()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AAgent::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAgent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAgent::Init(UClass* l_subjectClass, UGenotype* l_genotype, FVector location, FRotator rotation) {

	subjectClass = l_subjectClass;
	genotype = l_genotype;

	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	spawnParams.bNoFail = true;
	spawnParams.Owner = this;
	spawnParams.Instigator = NULL;

	subject = GetWorld()->SpawnActor(subjectClass, &location, &rotation, spawnParams);
	
	IAgentInterface::Execute_Init(subject, genotype);
	subject->OnEndPlay.AddDynamic(this, &AAgent::OnSubjectDied);

	lastSubjectEvaluation = 0.0f;
}

void AAgent::checkSubjectEvaluation() {
	float evaluation = IAgentInterface::Execute_getEvaluation(subject);
	//UE_LOG(LogTemp, Warning, TEXT("Evaluation: %f, lastEval: %f, delta: %f"), evaluation, lastSubjectEvaluation, evaluation - lastSubjectEvaluation);
	if (evaluation - lastSubjectEvaluation < 250.0f) subject->Destroy();
	lastSubjectEvaluation = evaluation;
}

void AAgent::OnSubjectDied(AActor* agent, EEndPlayReason::Type reason) {
	if (reason == EEndPlayReason::Destroyed && agent == subject) {
		GetWorld()->GetTimerManager().ClearTimer(evaluationCheckHandle);
		genotype->evaluation = IAgentInterface::Execute_getEvaluation(subject);
		agentDied.Broadcast(subject);
		Destroy();
	}
}

