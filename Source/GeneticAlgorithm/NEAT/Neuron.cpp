#include "Neuron.h"

float Neuron::activationFunction(float input) {
	switch (activationFunc) {
	case EActivationFunc::Linear:
		return input;
		break;

	case EActivationFunc::ReLu:
		return (input < 0 ? 0 : input);
		break;

	case EActivationFunc::PReLu:
		return (input <= 0 ? (input / 10) : input);
		break;

	case EActivationFunc::Sigmoid:
		return 1 / (1 + exp(-input));
		break;

	case EActivationFunc::Tanh:
		return (exp(2 * input) - 1) / (exp(2 * input) + 1);
		break;
	}
	return input;
}

float Neuron::activate() {
	//UE_LOG(LogTemp, Warning, TEXT("Type: %d, output: %f"), (int) type, output);
	if (output != NAN || type == NodeType::INPUT) return output;
	if (type == NodeType::BIAS) return 1;

	output = 0;
	float inputSum = 0;
	for (TPair<Neuron*, float>& pair : incomingNeurons) {
		inputSum += pair.Key->activate() * pair.Value;
		/*float a = pair.Key->activate();
		UE_LOG(LogTemp, Warning, TEXT("NeuronReturn: %f, Weight: %f"), a, pair.Value);
		inputSum += a * pair.Value;*/
	}
	//UE_LOG(LogTemp, Warning, TEXT("Sum: %f"), inputSum);
	output = activationFunction(inputSum);
	//UE_LOG(LogTemp, Warning, TEXT("Output: %f"), output);
	return output;
}