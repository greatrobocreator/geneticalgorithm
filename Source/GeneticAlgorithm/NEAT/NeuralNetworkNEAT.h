// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Containers/Array.h"
#include "Containers/Map.h"
#include "Containers/Set.h"

#include "GenotypeNEAT.h"
#include "Neuron.h"

#include "NeuralNetworkNEAT.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GENETICALGORITHM_API UNeuralNetworkNEAT : public UActorComponent
{
	GENERATED_BODY()

protected:	

	TArray<Neuron*> neurons;
	TArray<Neuron*> inputNeurons;
	TArray<Neuron*> outputNeurons;

public:

	UNeuralNetworkNEAT();
	~UNeuralNetworkNEAT();

	UFUNCTION(BlueprintCallable)
	void Init(UGenotypeNEAT* genotype);

	UFUNCTION(BlueprintCallable, meta = (AutoCreateRefTerm = "inputs"))
	TArray<float> Process(const TArray<float>& inputs);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
