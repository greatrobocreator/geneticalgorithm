#pragma once

#include "CoreMinimal.h"
#include "Containers/Array.h"

#include "GenotypeNEAT.h"

class Species
{
public:

	float maxEvaluation;

	// Age of last max evaluation improvement
	int lastImprovementAge;

	// Age of the species
	int age;

	TArray< UGenotypeNEAT* > population;

	Species() : maxEvaluation(0), lastImprovementAge(0), age(0) {};
	~Species() { population.Empty(); };
};

