// Fill out your copyright notice in the Description page of Project Settings.


#include "GenotypeNEAT.h"

UGenotypeNEAT::UGenotypeNEAT(UGenotypeNEAT* genotype) : UGenotype(genotype) {
	Init(genotype);
}

void UGenotypeNEAT::Init(UGenotypeNEAT* genotype) {
	if (genotype != NULL) {
		Init(genotype->nodeGenes, genotype->linkGenes);
	}
}

void UGenotypeNEAT::Init(TArray<NodeGene>& l_nodeGenes, TArray<LinkGene>& l_linkGenes) {
	// copying genes
	nodeGenes = l_nodeGenes;
	linkGenes = l_linkGenes;
}

void UGenotypeNEAT::Init(int inputSize, int outputSize, TArray<Innovation>& innovations) {
	nodeGenes.Empty();
	linkGenes.Empty();

	for (int i = 0; i < inputSize; ++i) {
		nodeGenes.Add(NodeGene(NodeType::INPUT, innovations.Num()));
		innovations.Add(Innovation(NodeType::INPUT));
	}
	nodeGenes.Add(NodeGene(NodeType::BIAS, innovations.Num()));
	innovations.Add(Innovation(NodeType::BIAS));
	for (int i = 0; i < outputSize; ++i) {
		nodeGenes.Add(NodeGene(NodeType::OUTPUT, innovations.Num()));
		innovations.Add(Innovation(NodeType::OUTPUT));
	}
	for (int i = 0; i < inputSize + 1; ++i) {
		for (int j = inputSize + 1; j < inputSize + 1 + outputSize; ++j) { // + 1 because of BIAS
			linkGenes.Add(LinkGene(nodeGenes[i].innovationNumber, nodeGenes[j].innovationNumber, 0.0, innovations.Num()));
			innovations.Add(Innovation(linkGenes.Last().inputInnov, linkGenes.Last().outputInnov));
		}
	}
}


void UGenotypeNEAT::randomizeGenotype(float minValue, float maxValue) {
	for (int i = 0; i < linkGenes.Num(); ++i)
		linkGenes[i].weight = FMath::FRandRange(minValue, maxValue);
}