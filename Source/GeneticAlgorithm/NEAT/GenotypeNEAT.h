// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "../Genotype.h"

#include "GenotypeNEAT.generated.h"

enum NodeType {
	INPUT,
	OUTPUT,
	BIAS,
	HIDDEN
};

typedef struct NodeGene {
	int innovationNumber;
	EActivationFunc function;
	NodeType type;

	NodeGene(NodeType l_type, int l_innovationNumber, EActivationFunc l_function = EActivationFunc::Tanh) : type(l_type), innovationNumber(l_innovationNumber), function(l_function) {};
	//NodeGene() : NodeGene(NodeType::HIDDEN, -1) {};

	virtual bool operator < (const NodeGene& r) const { return innovationNumber < r.innovationNumber; };
} NodeGene;

typedef struct LinkGene {
	int inputInnov, outputInnov, innovationNumber;
	float weight;
	bool enabled;

	LinkGene(int l_inputInnov, int l_outputInnov, float l_weight, int l_innovationNumber, bool l_enabled = true) : inputInnov(l_inputInnov), outputInnov(l_outputInnov), weight(l_weight), innovationNumber(l_innovationNumber), enabled(l_enabled) {};

	virtual bool operator < (const LinkGene& r) const { return innovationNumber < r.innovationNumber; };
} LinkGene;

enum InnovationType { NODE, LINK };

typedef struct Innovation {
	InnovationType type;
	NodeType nodeType;
	int inputInnov, outputInnov;
	int replacedLinkInnov;

	// Node constructor
	Innovation(NodeType l_nodeType, int l_replacedLinkInnov = -1) : type(InnovationType::NODE), nodeType(l_nodeType), replacedLinkInnov(l_replacedLinkInnov) {};

	// Link constructor
	Innovation(int l_inputInnov, int l_outputInnov) : type(InnovationType::LINK), inputInnov(l_inputInnov), outputInnov(l_outputInnov) {};
	Innovation(LinkGene& link) : Innovation(link.inputInnov, link.outputInnov) {};

} Innovation;

UCLASS(Blueprintable)
class GENETICALGORITHM_API UGenotypeNEAT : public UGenotype
{
	GENERATED_BODY()

public:
	// variables
	TArray< NodeGene > nodeGenes;
	TArray< LinkGene > linkGenes;

	// constructors & destructors
	UGenotypeNEAT() {};
	// copy other genotype
	UGenotypeNEAT(UGenotypeNEAT* genotype);
	UGenotypeNEAT(UGenotype* genotype) : UGenotypeNEAT((UGenotypeNEAT*) genotype) {};

	// functions

	// initialization
	// from other genotype
	void Init(UGenotypeNEAT* genotype);
	// using genes
	void Init(TArray<NodeGene>& l_nodeGenes, TArray<LinkGene>& l_linkGenes);
	// construct fully connected genotype without hidden layers by input and output sizes
	void Init(int inputSize, int outputSize, TArray<Innovation>& innovations);

	void randomizeGenotype(float minValue, float maxValue);
};
