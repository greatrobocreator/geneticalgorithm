#pragma once

#include "CoreMinimal.h"
#include "Containers/Array.h"
#include "Containers/Map.h"

#include "GenotypeNEAT.h"
#include "../ActivationFunc.h"

class Neuron
{
public:
	TArray< TPair<Neuron*, float> > incomingNeurons; // incoming neurons pointers with weights of links to them
	float output = NAN;
	EActivationFunc activationFunc;
	NodeType type;

	Neuron(TArray< TPair< Neuron*, float > >& l_incomingNeurons, NodeType l_type, EActivationFunc l_activationFunc = EActivationFunc::Tanh) : incomingNeurons(l_incomingNeurons), type(l_type), activationFunc(l_activationFunc) {};
	Neuron(NodeType l_type, EActivationFunc l_activationFunc = EActivationFunc::Tanh) : type(l_type), activationFunc(l_activationFunc) {};

	float activate();

private:
	float activationFunction(float input);
};

