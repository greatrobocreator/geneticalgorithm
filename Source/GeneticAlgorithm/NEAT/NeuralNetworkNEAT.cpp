// Fill out your copyright notice in the Description page of Project Settings.


#include "NeuralNetworkNEAT.h"

// Sets default values for this component's properties
UNeuralNetworkNEAT::UNeuralNetworkNEAT()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

UNeuralNetworkNEAT::~UNeuralNetworkNEAT() {
	neurons.Empty();
	inputNeurons.Empty();
	outputNeurons.Empty();
}

// Called when the game starts
void UNeuralNetworkNEAT::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UNeuralNetworkNEAT::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UNeuralNetworkNEAT::Init(UGenotypeNEAT* genotype) {
	/*TMap<int, TArray< LinkGene > > outgoingNodes; // Map for node innovation number and its outgoing links
	TMap<int, TArray< TPair<Neuron*, float> > > incomingNeurons; // Map for node innovation number and its incoming links
	TQueue<NodeGene> queue; // Queue for bfs for nodes initializing
	TSet<int> visited; // Set of visited nodes

	for (LinkGene& link : genotype->linkGenes)
		outgoingNodes.FindOrAdd(link.inputInnov).Add(link);

	for (NodeGene& node : genotype->nodeGenes)
		if (node.type == NodeType::INPUT || node.type == NodeType::BIAS)
			queue.Enqueue(node);

	while (!queue.IsEmpty()) {
		NodeGene* node = queue.Peek();

		if (!visited.Contains(node->innovationNumber)) {
			if (node->type == NodeType::INPUT) {
				neurons.Add(new Neuron(node->type));
				inputNeurons.Add(neurons.Last());
			}
			else {
				neurons.Add(new Neuron(incomingNeurons.FindOrAdd(node->innovationNumber), node->type, node->function));
			}
			if (node->type == NodeType::OUTPUT) outputNeurons.Add(neurons.Last());

			for (LinkGene& link : outgoingNodes.FindOrAdd(node->innovationNumber)) {
				incomingNeurons.FindOrAdd(link.outputInnov).Add(TPair<Neuron*, float>(neurons.Last(), link.weight));
				queue.Enqueue(link);
			}
			visited.Add(node->innovationNumber);
		}
		queue.Pop();
	}
	int counter = 0;
	for (int i = 0; i < neurons.Num(); ++i) {
		i++;
		counter += counter;
	}
	for (int i = 0; i < inputNeurons.Num(); ++i) {
		counter += counter;
		i++;
	}
	for (int i = 0; i < outputNeurons.Num(); ++i) {
		counter += counter;
		i++;
	}*/
	neurons.Empty();
	inputNeurons.Empty();
	outputNeurons.Empty();

	TMap<int, Neuron*> neuronsMap;
	
	for (NodeGene& node : genotype->nodeGenes) {
		neurons.Add(new Neuron(node.type, node.function));
		if (node.type == NodeType::INPUT) inputNeurons.Add(neurons.Last());
		else if (node.type == NodeType::OUTPUT) outputNeurons.Add(neurons.Last());

		neuronsMap.Add(node.innovationNumber, neurons.Last());
	}
	for (LinkGene& link : genotype->linkGenes) {
		neuronsMap.FindOrAdd(link.outputInnov)->incomingNeurons.Add(TPair<Neuron*, float>(neuronsMap.FindOrAdd(link.inputInnov), link.weight));
	}
	/*for (Neuron* neuron : neurons)
		UE_LOG(LogTemp, Warning, TEXT("Incoming: %d"), neuron->incomingNeurons.Num());*/
}

TArray<float> UNeuralNetworkNEAT::Process(const TArray<float>& inputs) {

	for (Neuron* neuron : neurons) neuron->output = NAN;

	for (int i = 0; i < inputs.Num() && i < inputNeurons.Num(); ++i)
		inputNeurons[i]->output = inputs[i];

	TArray<float> outputs;
	for (int i = 0; i < outputNeurons.Num(); ++i)
		outputs.Add(outputNeurons[i]->activate());

	for (Neuron* neuron : neurons) neuron->output = NAN;

	return outputs;
}