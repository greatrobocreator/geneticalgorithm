#pragma once

#include "CoreMinimal.h"
#include "Math/UnrealMathUtility.h"

#include "ActivationFunc.h"

class NeuralLayer
{
private:
		float** weights;
		size_t size;
		size_t inputSize;
		EActivationFunc activationFunc;
		//bool inputLayer = false;

public:

	NeuralLayer();
	~NeuralLayer();// = default;

	NeuralLayer(size_t l_size, size_t l_inputSize, EActivationFunc l_activationFunc);
	NeuralLayer(size_t l_size, size_t l_inputSize, float** l_weights, EActivationFunc l_activationFunc);
	NeuralLayer(size_t l_size, size_t l_inputSize, float* l_weights, EActivationFunc l_activationFunc);
	
	/// //for output layer
	///NeuralLayer(size_t l_size, EActivationFunc l_activationFunc);
	///NeuralLayer(size_t l_size, float** l_weights, EActivationFunc l_activationFunc);


	float ActivateNeuron(float input);
	void printWeights();

	//TArray<float> Process(TArray<float>& input);
	float* Process(float* input);

	size_t getSize() { return size; }
	size_t getInputSize() { return inputSize; }
	EActivationFunc getActivationFunc() { return activationFunc; }
	float** getWeights() { return weights;  }
	//bool isOutput() { return outputLayer; }

};

