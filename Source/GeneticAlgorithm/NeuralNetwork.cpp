
#include "NeuralNetwork.h"


UNeuralNetwork::UNeuralNetwork() : UNeuralNetwork(layersSizes, layersFuncs)
{

	//UE_LOG(LogTemp, Warning, TEXT("InEmptyConstructor"));

}

UNeuralNetwork::UNeuralNetwork(TArray< int >& l_layersSizes, TArray< EActivationFunc >& l_layersFuncs) : layersSizes(l_layersSizes), layersFuncs(l_layersFuncs)
{

	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	//UE_LOG(LogTemp, Warning, TEXT("InNormalCOnstructor"));

	Init();

}


void UNeuralNetwork::Init() {

	if (layersSizes.Num() == layersFuncs.Num() + 1) {

		if (layersSizes.Num() > 0) {
			layers = new NeuralLayer[layersSizes.Num() - 1];

			int i = 1;
			for (; i < layersSizes.Num(); ++i) {
				layers[i - 1] = NeuralLayer(layersSizes[i], layersSizes[i - 1], layersFuncs[i - 1]);
			}
		}
	}
	else if(!(layersSizes.Num() == 0 && layersFuncs.Num() == 0)) {
		UE_LOG(LogTemp, Error, TEXT("Failed to init. Activation functions count must be one less then layers count"));
	}

}

void UNeuralNetwork::Init(UGenotypeFC* genotype) {//, const TArray< int >& l_layersSizes, const TArray< EActivationFunc >& l_layersFuncs) {

	layersSizes = genotype->layersSizes;
	layersFuncs = genotype->layersFuncs;

	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Genotype params count: %d"), genotype->parametersCount));

	if (layersSizes.Num() == layersFuncs.Num() + 1) {

		if (layersSizes.Num() > 0) {
			layers = new NeuralLayer[layersSizes.Num() - 1];

			int i = 1;
			int j = 0;
			for (; i < layersSizes.Num(); ++i) {
				layers[i - 1] = NeuralLayer(layersSizes[i], layersSizes[i - 1], (genotype->parameters + j), layersFuncs[i - 1]);
				j += layersSizes[i] * (layersSizes[i - 1] + 1);
			}
		}
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("Failed to init. Activation functions count must be one less then layers count"));
	}
}

void UNeuralNetwork::printLayersWeights() {

	for (int i = 0; i < layersSizes.Num(); ++i) {
		layers[i].printWeights();
	}

}

TArray<float> UNeuralNetwork::getParameters() {

	TArray<float> result;

	for (int i = 0; i < layersSizes.Num() - 1; ++i) {
		for (int j = 0; j < layers[i].getSize(); ++j) {
			for (int k = 0; k <= layers[i].getInputSize(); ++k) {
				result.Add(layers[i].getWeights()[j][k]);
			}
		}
	}

	return result;
}

// Called when the game starts
void UNeuralNetwork::BeginPlay()
{
	Super::BeginPlay();

	Init();

	//GetWorld()->GetTimerManager().SetTimer(WeightsTimerHandle, this, &UNeuralNetwork::printLayersWeights, 20.0f, true, 2.0f);
}

TArray<float> UNeuralNetwork::Process(const TArray<float>& input) {

	/*UE_LOG(LogTemp, Warning, TEXT("Processing NN"));
	UE_LOG(LogTemp, Warning, TEXT("LayersSizes: %d"), layers.Num());*/


	float* output = new float[input.Num()];

	for (int i = 0; i < input.Num(); ++i) {
		output[i] = input[i];
	}

	TArray<float> outputT;
	if (layersSizes.Num() > 0) {
		for (int i = 0; i < layersSizes.Num() - 1; ++i) {
			output = layers[i].Process(output);
			//UE_LOG(LogTemp, Warning, TEXT("Output of Layer %d: %f"), i, output[0]);
		}

		for (int i = 0; i < layers[layersSizes.Num() - 2].getSize(); ++i) { // -1 for last in array and -1 cause no input layers in layers array
			outputT.Add(output[i]);
		}
	}

	return outputT;
}

// Called every frame
void UNeuralNetwork::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

