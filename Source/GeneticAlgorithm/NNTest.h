// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Containers/Array.h"

#include "GenotypeFC.h"
#include "NEAT/NeuralNetworkNEAT.h"
#include "GeneticAlgorithm.h"

#include "NNTest.generated.h"

UCLASS()
class GENETICALGORITHM_API ANNTest : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ANNTest();

	UPROPERTY(BlueprintReadWrite)
	UGenotypeNEAT* genotype;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
