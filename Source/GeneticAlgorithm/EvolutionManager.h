// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/TargetPoint.h"
#include "Engine/World.h"

#include "GeneticAlgorithm.h"
#include "NeuralLayer.h"
#include "Agent.h"

#include "EvolutionManager.generated.h"

DECLARE_EVENT(AEvolutionManager, FAllAgentsDied)

UCLASS()
class GENETICALGORITHM_API AEvolutionManager : public AActor
{
	GENERATED_BODY()
	
	// variables
private:
	UPROPERTY(EditAnywhere)
	int populationSize = 30;

	UPROPERTY(EditAnywhere)
	int restartAfter = 100;

	UPROPERTY(EditAnywhere)
	int elitistSelection = false;

	UPROPERTY(EditAnywhere)
	bool neat = false;

	UPROPERTY(EditAnywhere)
	TArray< int > layersSizes;

	UPROPERTY(EditAnywhere)
	TArray< EActivationFunc > layersFuncs;

	TArray< AAgent* > agents;

	FTimerHandle restartTimerHandle;
	FTimerHandle evaluationCheckHandle;
	FAllAgentsDied AllAgentsDied;

public:

	UPROPERTY(BlueprintReadOnly)
	UGeneticAlgorithm* geneticAlgorithm;

	UPROPERTY(EditAnywhere)
	ATargetPoint* carsSpawnPoint;

	UPROPERTY(EditAnywhere)
	UClass* subjectClass;

	UPROPERTY(EditAnywhere)
	float evaluationCheckTime = 3.0f;

	UPROPERTY(EditAnywhere)
	float minAgentsDeltaEvaluation = 0.2f;

	int agentsAlive;

	// Constructors
	AEvolutionManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Methods
	UFUNCTION(BlueprintCallable)
	void startEvolution();

private:
	UFUNCTION(BlueprintCallable)
	bool checkGenerationTermination(TArray<UGenotype*>& currentPopulation);

	/*UFUNCTION(BlueprintCallable)
	void checkAgentsEvaluation();*/

	void OnGATermination(UGeneticAlgorithm* l_geneticAlgorithm);

	void restartAlgorithm(float wait);

	UFUNCTION(BlueprintCallable)
	void startEvaluation(TArray<UGenotype*>& currentPopulation);

	UFUNCTION(BlueprintCallable)
	void OnAgentDied(AActor* agent);

public:

	// getters-setters
	int getGenerationCount() { return geneticAlgorithm->generationCount; };
};
