#pragma once

UENUM(BlueprintType)
enum class EActivationFunc : uint8 {
	ReLu UMETA(DisplayName = "ReLu"),
	PReLu UMETA(DisplayName = "PReLu"),
	Sigmoid UMETA(DisplayName = "Sigmoid"),
	Tanh UMETA(DisplayName = "Tanh"),
	Linear UMETA(DisplayName = "Linear")
};
