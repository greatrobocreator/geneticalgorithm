// Fill out your copyright notice in the Description page of Project Settings.


#include "EvolutionManager.h"

// Sets default values
AEvolutionManager::AEvolutionManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	//if (subjectClass == NULL || !subjectClass->ImplementsInterface(UAgentInterface::StaticClass())) subjectClass = UAgentInterface::StaticClass();

}

// Called when the game starts or when spawned
void AEvolutionManager::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEvolutionManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	/*if(IsValid(geneticAlgorithm))
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, geneticAlgorithm->GetName());*/
}

// Starts evolutionary process
void AEvolutionManager::startEvolution() {


	// Setup genetic algorithm
	geneticAlgorithm = NewObject<UGeneticAlgorithm>();
	geneticAlgorithm->Init(layersSizes, layersFuncs, populationSize, neat);
	geneticAlgorithm->evaluation.BindDynamic(this, &AEvolutionManager::startEvaluation);
	
	if (!elitistSelection) {
		geneticAlgorithm->selection.BindDynamic(geneticAlgorithm, &UGeneticAlgorithm::remainderStochasticSampling);
	}

	geneticAlgorithm->recombination.BindDynamic(geneticAlgorithm, &UGeneticAlgorithm::randomRecombination);
	geneticAlgorithm->mutation.BindDynamic(geneticAlgorithm, &UGeneticAlgorithm::mutateAllButBestTwo);

	AllAgentsDied.AddUObject(geneticAlgorithm, &UGeneticAlgorithm::evaluationFinished);

	if (restartAfter > 0) {
		geneticAlgorithm->terminationCriterion.BindDynamic(this, &AEvolutionManager::checkGenerationTermination);
		geneticAlgorithm->AlgorithmTerminated.AddUObject(this, &AEvolutionManager::OnGATermination);
	}

	geneticAlgorithm->start();
}

bool AEvolutionManager::checkGenerationTermination(TArray<UGenotype*>& currentPopulation) {
	return (restartAfter > 0) && (geneticAlgorithm->generationCount >= restartAfter);
}

// To be called when the genetic algorithm was terminated
void AEvolutionManager::OnGATermination(UGeneticAlgorithm* l_geneticAlgorithm) {

	AllAgentsDied.Clear();
	restartAlgorithm(5.0f);

}

// Restarts the algorithm after a specific wait time second wait
void AEvolutionManager::restartAlgorithm(float wait) {
	GetWorld()->GetTimerManager().SetTimer(restartTimerHandle, this, &AEvolutionManager::startEvolution, 0.0f, false, wait);
}

/*void AEvolutionManager::checkAgentsEvaluation() {
	int count = 0;
	GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, TEXT("Checking agents"));
	UE_LOG(LogTemp, Warning, TEXT("Checking agents"));
	for (int i = 0; i < agents.Num(); ++i) {
		if (IsValid(agents[i]) && IsValid(agents[i]->subject)) {
			float evaluation = IAgentInterface::Execute_getEvaluation(agents[i]->subject);
			if (evaluation - agents[i]->lastSubjectEvaluation < minAgentsDeltaEvaluation) {
				//GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, TEXT("Destroying agent"));
				//UE_LOG(LogTemp, Warning, TEXT("Destroying agent"));
				agents[i]->subject->Destroy();
			}
			else count++;
			agents[i]->lastSubjectEvaluation = evaluation;
		}
	}
	if(count <= 0)
		GetWorld()->GetTimerManager().ClearTimer(evaluationCheckHandle);
}*/

void AEvolutionManager::startEvaluation(TArray<UGenotype*>& currentPopulation) {

	agents.Empty();
	agentsAlive = 0;
	GetWorld()->GetTimerManager().ClearTimer(evaluationCheckHandle);

	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	spawnParams.bNoFail = true;
	spawnParams.Owner = this;
	spawnParams.Instigator = NULL;

	FVector location = carsSpawnPoint->GetActorLocation();
	FRotator rotation = carsSpawnPoint->GetActorRotation();

	// Spawning agents
	for (UGenotype* genotype : currentPopulation) {
		AAgent* agent = GetWorld()->SpawnActor<AAgent>(location, rotation, spawnParams);
		agent->Init(subjectClass, genotype, location, rotation);
		agent->agentDied.AddUObject(this, &AEvolutionManager::OnAgentDied);
		agents.Add(agent);
		agentsAlive++;

		// Setting timer for deleting bad agents
		GetWorld()->GetTimerManager().SetTimer(agent->evaluationCheckHandle, agent, &AAgent::checkSubjectEvaluation, evaluationCheckTime, true, 5.0f);
	}


	//GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, TEXT("Setting new timer"));
	//UE_LOG(LogTemp, Warning, TEXT("Setting new timer"));
	// Setting timer for deleting bad agents
	//GetWorld()->GetTimerManager().SetTimer(evaluationCheckHandle, this, &AEvolutionManager::checkAgentsEvaluation, evaluationCheckTime, true, 5.0f);

}

void AEvolutionManager::OnAgentDied(AActor* agent) {

	/*GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, TEXT("Agent died"));
	UE_LOG(LogTemp, Warning, TEXT("Agent died"));*/
	agentsAlive--;
	if (agentsAlive <= 0) {
		//	GetWorld()->GetTimerManager().ClearTimer(evaluationCheckHandle);
		AllAgentsDied.Broadcast();
	}
}